<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable=['title','description','image','price'];
    
    public function orders(){
    	return $this->belongsToMany(Order::class,'cart_items');
    }
}
