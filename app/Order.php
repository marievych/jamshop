<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function items(){
    	return $this->hasMany(CartItem::class);
    }
    
    public function total(){
    	$total=0;
	    foreach ($this->items as $item) {
		    $total+=$item->quantity*$item->price;
    	}
    	return $total;
    }
    
    public function quantity(){
    	$quantity=0;
	    foreach ($this->items as $item) {
		    $quantity+=$item->quantity;
    	}
    	return $quantity;
    }
    
    public function user(){
    	return $this->belongsTo(User::class);
    }
    
    public function products(){
    	return $this->belongsToMany(Product::class)->using(CartItem::class);
    }
}
