<?php
/**
 * Created by PhpStorm.
 * User: Yevhen.Marievych
 * Date: 23.03.2017
 * Time: 16:24
 */

namespace App;

use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class Cart
{
	
	public $items = [];
	
	
	static function create($session)
	{
		$cart = new Cart($session);
		dd($cart);
	}
	
	public function add($id)
	{
		$product = Product::findOrFail($id);
		if($product->active) {
			if (count($this->items) && array_has($this->items, $id)) {
				++$this->items[$id];
			} else {
				$this->items[$id] = 1;
			}
			$this->session();
		}
		else throw new BadRequestHttpException();
	}
	
	public function remove($id)
	{
		if (array_has($this->items,$id)&&($this->items[$id]>1)) {
			--$this->items[$id];
		} else {
			unset($this->items[$id]);
		}
		$this->session();
	}
	
	public function removeAll($id)
	{
		if (array_has($this->items, $id)) {
			unset($this->items[$id]);
		}
		$this->session();
	}
	
	public function total()
	{
		$total = 0;
		$products = Product::whereIn('id', array_keys($this->items));
		foreach ($products as $product) {
			$total += $product->price * $this->items[$product->id];
		}
		return $total;
	}
	
	public static function get()
	{
		if (Session::has('cart')) {
			$cart = Session::get('cart');
			//dd($cart);
		} else {
			$cart = new Cart();
		}
		//dd($cart);
		return $cart;
	}
	
	public function session()
	{
		if(count($this->items)>0) {
			$total = 0;
			$products = Product::whereIn('id', array_keys($this->items))->get();
			foreach ($products as $product) {
				$total += $product->price * $this->items[$product->id];
			}
			$this->total = $total;
			$this->quantity = array_sum($this->items);
			Session::put('cart', $this);
		}
		else{
			Session::remove('cart');
		}
	}
}