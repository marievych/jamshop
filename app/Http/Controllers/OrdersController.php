<?php

namespace App\Http\Controllers;

use App\Cart;
use App\CartItem;
use App\Contact;
use App\Order;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class OrdersController extends Controller
{
	
	public function getCart()
	{
		$cart = Cart::get();
		$user = User::whereId(Auth::user()->id)->with('contacts')->first();
		$products = Product::whereIn('id', array_keys($cart->items))->get();
		return view('orders.cart', ['cart' => $cart, 'products' => $products, 'user' => $user]);
	}
	
	public function postCart(Request $request)
	{
		$user = Auth::user();
		$validator = Validator::make($request->all(),[
			'city'=>'required',
			'address'=>'required',
			'phone'=>'required'
		]);
		if ($validator->passes()) {
			$contact = new Contact();
			$contact->city = $request->city;
			$contact->address = $request->address;
			$contact->phone = $request->phone;
			$contact->user_id = $user->id;
			$contact->save();
		} else {
			if (!($request->contacts&&$contact = Contact::find($request->contacts))){
				return redirect()->back();
			}
		}
		$cart=Cart::get();
		$order=new Order();
		$order->user_id=Auth::user()->id;
		$order->city=$contact->city;
		$order->address=$contact->address;
		$order->phone=$contact->phone;
		$order->is_paid=false;
		$order->is_shipped=false;
		$order->save();
		foreach ($cart->items as $id => $quantity) {
			$cartItem = new CartItem();
			$cartItem->order_id = $order->id;
			$cartItem->product_id = $id;
			$cartItem->price = Product::find($id)->price;
			$cartItem->quantity = $quantity;
			$cartItem->save();
			$cart->removeAll($id);
		}
		$order=Order::with(['items'])->whereId($order->id)->first();
		return view('orders.checkout',['order'=>$order]);
	}
	
	public function getAdd(Request $request, $id)
	{
		$cart = Cart::get();
		$cart->add($id);
		return redirect()->back();
	}
	
	public function getRemove(Request $request, $id)
	{
		$cart = Cart::get();
		$cart->remove($id);
		return redirect()->back();
	}
	
	public function getRemoveAll(Request $request, $id)
	{
		$cart = Cart::get();
		$cart->removeAll($id);
		return redirect()->back();
	}
	public function getDetails($id){
		$order=Order::whereId($id)->with('items.product')->first();
		return view('orders.details',['order'=>$order]);
	}
	
	public function getList()
	{
		$orders = Order::whereUserId(Auth::user()->id)->with('items.product')->paginate(10);
		return view('orders.list', ['orders' => $orders]);
	}
}
