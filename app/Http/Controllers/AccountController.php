<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AccountController extends Controller
{
	public function getDetails()
	{
		$user = Auth::user();
		return view('account.details', ['user' => $user]);
	}
	
	public function getEdit()
	{
		$user = Auth::user();
		return view('account.edit', ['user' => $user]);
	}
	
	public function postEdit(Request $request){
		$user=Auth::user();
		$this->validate($request,[
			'name'=>'required',
			'email'=>'email|unique:users,email,'.$user->id
		]);
		$user->name=$request->name;
		$user->email=$request->email;
		$user->save();
		return redirect()->route('account');
	}
}
