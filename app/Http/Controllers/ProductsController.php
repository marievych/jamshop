<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductsController extends Controller
{
	public function getList()
	{
		$products = Product::whereActive(true)->get();
		return view('products.list', ['products' => $products]);
	}
	
	public function getDetails($id){
		$product=Product::findOrFail($id);
		if($product->active||(Auth::user()&&Auth::user()->isAdmin())) {
			return view('products.details', ['product' => $product]);
		}
		throw new NotFoundHttpException();
	}
	
}
