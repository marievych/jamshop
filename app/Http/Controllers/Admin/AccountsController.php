<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Ultraware\Roles\Models\Role;

class AccountsController extends Controller
{
	public function getList()
	{
		$users = User::with(['roles', 'orders'])->get();
		return view('admin.accounts.list', ['users' => $users]);
	}
	
	public function getOrders($id)
	{
		$user = User::whereId($id)->with('orders.items')->first();
		return view('admin.accounts.orders', ['user' => $user]);
	}
	
	public function getDetails($id)
	{
		$user = User::findOrFail($id);
		return view('admin.accounts.details', ['user' => $user]);
	}
	
	public function getEdit($id)
	{
		$user = User::findOrFail($id);
		$roles = Role::all();
		return view('admin.accounts.edit', ['user' => $user, 'roles' => $roles]);
	}
	
	public function postEdit(Request $request, $id)
	{
		$user = User::findOrFail($id);
		$this->validate($request, ['name' => 'required', 'email' => 'email|unique:users,email,' . $user->id, 'roles' => 'required']);
		$user->name = $request->name;
		$user->email = $request->email;
		$user->syncRoles($request->roles);
		$user->save();
		return redirect()->route('admin.account.details', ['id' => $id]);
	}
}
