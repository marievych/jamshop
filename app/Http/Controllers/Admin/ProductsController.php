<?php

namespace App\Http\Controllers\Admin;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller
{
	public function getList(){
		$products=Product::paginate(10);
		return view('admin.products.list',['products'=>$products]);
	}
	
	public function getCreate(){
		$product=new Product();
		return view('admin.products.edit',['product'=>$product]);
	}
	
	public function getEdit($id){
		$product=Product::findOrFail($id);
		return view('admin.products.edit',['product'=>$product]);
	}
	
	public function postCreate(Request $request){
		$this->validate($request,['title'=>'required','description'=>'required','price'=>'required|numeric']);
		$extension=$request->image->getClientOriginalExtension();
		$filename=sha1(time().time()).'.'.$extension;
		$request->image->storeAs('public',$filename);
		$product=new Product($request->all());
		$product->image=Storage::url($filename);
		$product->active=$request->active;
		$product->save();
		return redirect()->route('admin.products',['id'=>$product->id]);
	}
	public function postEdit(Request $request,$id){
		$this->validate($request,['title'=>'required','description'=>'required','price'=>'required|numeric']);
		$product=Product::findOrFail($id);
		if($request->image) {
			$extension = $request->image->getClientOriginalExtension();
			$filename = sha1(time() . time()) . '.' . $extension;
			$request->image->storeAs('public', $filename);
			$product->image=Storage::url($filename);
		}
		$product->active=isset($request->active)?true:false;
		$product->title=$request->title;
		$product->description=$request->description;
		$product->price=$request->price;
		$product->save();
		return redirect()->route('admin.products',['id'=>$id]);
	}
	public function getDelete($id){
		$product=Product::findOrFail($id);
		return view('admin.products.delete',['product'=>$product]);
	}
	
	public function postDelete(Request $request,$id){
		$product=Product::findOrFail($id);
		if($product->orders()->count()>0){
			Session::flash('message',['text'=>$product->title.' can\'t be deleted.','type'=>'danger']);
			return redirect()->back();
		}
		$product->delete();
		Session::flash('message',['text'=>$product->title.' was deleted.','type'=>'primary']);
		return redirect()->route('admin.products');
	}
}
