<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\Status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
	public function getIndex()
	{
		$orders = Order::with(['user','items.product'])->paginate(10);
		return view('admin.orders.list', ['orders' => $orders]);
	}
	public function getEdit($id){
		$order=Order::findOrFail($id);
		return view('admin.orders.edit',['order'=>$order]);
	}
	
	public function postEdit(Request $request,$id){
		return redirect()->route('admin.order.details');
	}
	
	public function getDetails($id){
		$order=Order::with(['items.product'])->first();
		return view('admin.orders.details',['order'=>$order]);
	}
}
