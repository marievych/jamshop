<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    public function __construct()
    {
	    $this->quantity=0;
    }
    
    public function order(){
    	return $this->belongsTo(Order::class);
    }
    
    public function product(){
    	return $this->hasOne(Product::class,'id','product_id');
    }
}
