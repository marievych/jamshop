<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', ['uses' => 'ProductsController@getList', 'as' => 'products.list']);

Route::get('product/{id}', ['uses' => 'ProductsController@getDetails', 'as' => 'product.details']);

Route::get('account', ['uses' => 'UserController@getAccount', 'as' => 'account']);

Route::get('orders', ['uses' => 'OrdersController@getList', 'as' => 'orders'])->middleware(['auth']);

Route::get('cart', ['uses' => 'OrdersController@getCart', 'as' => 'cart'])->middleware(['auth']);

Route::post('cart', ['uses' => 'OrdersController@postCart', 'as' => 'cart'])->middleware('auth');

Route::get('order/add/{id}', ['uses' => 'OrdersController@getAdd', 'as' => 'order.add']);

Route::get('order/remove/{id}', ['uses' => 'OrdersController@getRemove', 'as' => 'order.remove']);

Route::get('order/remove-all/{id}', ['uses' => 'OrdersController@getRemoveAll', 'as' => 'order.removeAll']);

Route::get('order/{id}', ['uses' => 'OrdersController@getDetails', 'as' => 'order.details']);

Route::get('account', ['uses' => 'AccountController@getDetails', 'as' => 'account'])->middleware('auth');

Route::get('account/edit', ['uses' => 'AccountController@getEdit', 'as' => 'account.edit'])->middleware('auth');

Route::post('account/edit', ['uses' => 'AccountController@postEdit', 'as' => 'account.edit'])->middleware('auth');

Route::group(['namespace' => 'Admin', 'middleware' => 'role:admin'], function () {
	Route::get('admin/orders', ['uses' => 'OrdersController@getIndex', 'as' => 'admin.orders']);
	Route::get('admin/order/{id}', ['uses' => 'OrdersController@getDetails', 'as' => 'admin.order.details']);
	Route::get('admin/order/{id}/edit', ['uses' => 'OrdersController@getEdit', 'as' => 'admin.order.edit']);
	Route::post('admin/order/{id}/edit', ['uses' => 'OrdersController@postEdit', 'as' => 'admin.order.edit']);
	
	Route::get('admin/accounts', ['uses' => 'AccountsController@getList', 'as' => 'admin.accounts']);
	Route::get('admin/account/{id}/orders', ['uses' => 'AccountsController@getOrders', 'as' => 'admin.account.orders']);
	Route::get('admin/account/{id}/edit', ['uses' => 'AccountsController@getEdit', 'as' => 'admin.account.edit']);
	Route::post('admin/account/{id}/edit', ['uses' => 'AccountsController@postEdit', 'as' => 'admin.account.edit']);
	Route::get('admin/account/{id}', ['uses' => 'AccountsController@getDetails', 'as' => 'admin.account.details']);
	Route::get('admin/product/create', ['uses' => 'ProductsController@getCreate', 'as' => 'admin.product.create']);
	
	Route::post('admin/product/create', ['uses' => 'ProductsController@postCreate', 'as' => 'admin.product.create']);
	
	Route::get('admin/product/{id}/edit', ['uses' => 'ProductsController@getEdit', 'as' => 'admin.product.edit']);
	
	Route::post('admin/product/{id}/edit', ['uses' => 'ProductsController@postEdit', 'as' => 'admin.product.edit']);
	Route::get('admin/product/{id}/delete', ['uses' => 'ProductsController@getDelete', 'as' => 'admin.product.delete']);
	Route::post('admin/product/{id}/delete', ['uses' => 'ProductsController@postDelete', 'as' => 'admin.product.delete']);
	
	Route::get('admin/products', ['uses' => 'ProductsController@getList', 'as' => 'admin.products']);
});