<nav class="navbar navbar-toggleable-md navbar-light bg-faded fixed-top" >
	<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<a class="navbar-brand" href="{{route('products.list')}}">Jam shop</a>
	
	<div class="collapse navbar-collapse" id="navbarContent">
		<ul class="navbar-nav mr-auto">
		</ul>
		<ul class="navbar-nav">
			
			@if(Session::has('cart'))
				<li class="nav-item"><a class="nav-link" href="{{route('cart')}}"><i class="fa fa-shopping-cart fa-lg
				fa-fw"
				                                   aria-hidden="true"></i> Cart <span class="badge badge-default">{{Session::get('cart')->quantity}}</span></a></li>
			@endif
			<li class="nav-item dropdown">@if(Auth::guest())
				<li class="dropdown">
					<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
					   aria-haspopup="true"
					   aria-expanded="false"><i class="fa fa-user fa-lg" aria-hidden="true"></i> User</a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a class="nav-link" href="{{route('login')}}"><i class="fa fa-sign-in fa-lg fa-fw"
						                                    aria-hidden="true"></i>
								Login</a></li>
						<li><a class="nav-link" href="{{route('register')}}"><i class="fa fa-user-plus fa-lg fa-fw"
						                                       aria-hidden="true"></i>
								Register</a></li>
					</ul>
				</li>
			@else
				@if(Auth::user()->isAdmin())
					<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
					   aria-haspopup="true"
					   aria-expanded="false"><i class="fa fa-lock fa-lg" aria-hidden="true"></i> Admin</a>
					<ul class="dropdown-menu">
						<li>
							<a class="nav-link" href="{{route('admin.products')}}"><i class="fa fa-shopping-basket fa-lg fa-fw" aria-hidden="true"></i> Products</a>
						</li>
						<li>
							<a class="nav-link" href="{{route('admin.orders')}}"><i class="fa fa-shopping-cart fa-lg
							fa-fw"
							                                       aria-hidden="true"></i> Orders</a>
						</li>
						<li><a class="nav-link" href="{{route('admin.accounts')}}"><i class="fa fa-id-card-o fa-lg fa-fw"
						                                             aria-hidden="true"></i> Accounts</a></li>
					</ul>
				@endif
				<li class="dropdown">
					<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button"
					   aria-haspopup="true"
					   aria-expanded="false"><i class="fa fa-user fa-lg" aria-hidden="true"></i> User</a>
					<ul class="dropdown-menu dropdown-menu-right">
						<li><a class="nav-link" href="{{route('account')}}"><i class="fa fa-id-card-o fa-lg fa-fw"
						                                      aria-hidden="true"></i> Account</a></li>
						<li>
							<a class="nav-link" href="{{route('orders')}}"><i class="fa fa-shopping-cart fa-lg fa-fw"
							                                 aria-hidden="true"></i> Orders</a>
						</li>
						<li>
							<a class="nav-link" href="#" onclick="event.preventDefault();
                            $('#logout_form').submit();"><i class="fa fa-sign-out
                            fa-lg fa-fw" aria-hidden="true"></i>
								Logout</a>
							<form method="post" id="logout_form" action="{{route('logout')}}"
							      style="display: none">
								{{csrf_field()}}
							</form>
						</li>
					</ul>
				</li>
				@endif</li>
		</ul>
	</div>
</nav>