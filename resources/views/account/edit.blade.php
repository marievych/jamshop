@extends('layouts.master')

@section('title')
	Edit - {{$user->name}}
@endsection

@section('content')
	<h2>{{$user->name}}</h2>
	@if($errors->all())
	<ul>
		@foreach($errors->all() as $error)
			<li>{{$error}}</li>
			@endforeach
	</ul>
	@endif
	{{Form::model($user)}}
	<div class="form-group {{$errors->has('name')?'has-danger':''}}">
		{{Form::label('name','Name',['class'=>'form-control-label'])}}
		{{Form::text('name',null,['class'=>'form-control'])}}
	</div>
	<div class="form-group {{$errors->has('email')?'has-danger':''}}">
		{{Form::label('email','Email',['class'=>'form-control-label'])}}
		{{Form::text('email',null,['class'=>'form-control'])}}
	</div>
	<div class="form-group">
		{{Form::submit('Save',['class'=>'btn btn-success'])}}
	</div>
	{{Form::close()}}
@endsection