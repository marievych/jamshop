@extends('layouts.master')

@section('title')
	Account - {{$user->name}}
@endsection
@section('content')
	<table class="table">
		<tbody>
		<tr>
			<th>Id</th>
			<td>{{$user->id}}</td>
		</tr>
		<tr>
			<th>Name</th>
			<td>{{$user->name}}</td>
		</tr>
		<tr>
			<th>Email</th>
			<td>{{$user->email}}</td>
		</tr>
		</tbody>
	</table>
	<a href="{{route('account.edit')}}" class="btn btn-success">Edit</a>
@endsection