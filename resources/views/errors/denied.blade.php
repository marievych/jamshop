@extends('layouts.master')

@section('title')
	Access denied!
@endsection

@section('content')
	<div class="alert alert-danger">You don't have permission to access this page.</div>
@endsection