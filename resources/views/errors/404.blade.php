@extends('layouts.master')

@section('title')
	Ooops!
@endsection

@section('content')
	<div class="alert alert-danger">Ooops! Looks like the page you are looking for does not exist.</div>
@endsection