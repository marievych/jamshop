<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="icon" type="image/x-icon" href="{{asset('favicon.ico')}}">
    <link href="{{mix('css/app.css')}}" rel="stylesheet">
    <script>window.Laravel="{{json_encode(['csrToken'=>csrf_token()])}}"</script>
    @yield('styles')
</head>
<body>
@include('partials.header')
<div class="container" id="main">
    @if(Session::has('message'))
        <div class="alert alert-{{Session::get('message.type')}} alert-dismissible fade show" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            {{Session::get('message.text')}}
        </div>
        @endif
    @yield('content')
</div>
<script src="{{mix('js/app.js')}}"></script>
@yield('scripts')
</body>
</html>