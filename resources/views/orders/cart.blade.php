@extends('layouts.master')

@section('title')
	Shopping cart
@endsection
@section('content')
	<h2>Your cart</h2>
	@if(count($cart->items)>0)
		<table class="table">
			<thead>
			<tr>
				<th>Product</th>
				<th>Quantity</th>
				<th>Price</th>
				<th>Subtotal</th>
				<th></th>
			</tr>
			</thead>
			<tbody>
			@foreach($products as $product)
				<tr>
					<td>
						<a href="{{route('product.details',['id'=>$product->id])}}">{{$product->title}}</a>
					</td>
					<td>{{$cart->items[$product->id]}}</td>
					<td>{{$product->price}}&#8372;</td>
					<td>{{$cart->items[$product->id]*$product->price}}&#8372;</td>
					<td>
						<div class="btn-group">
							<button type="button" class="btn btn-sm btn-secondary dropdown-toggle"
							        data-toggle="dropdown"
							        aria-haspopup="true" aria-expanded="false">
								<i class="fa fa-pencil"
								   aria-hidden="true"></i> Actions
							</button>
							<ul class="dropdown-menu">
								<li><a class="dropdown-item" href="{{route('order.add',['id'=>$product->id])}}"><i
												class="fa fa-plus"
												aria-hidden="true"></i> Add one</a></li>
								<li><a class="dropdown-item" href="{{route('order.remove',['id'=>$product->id])}}"><i
												class="fa
									fa-minus"
												aria-hidden="true"></i>
										Remove one</a></li>
								<li role="separator" class="divider"></li>
								<li><a class="dropdown-item" href="{{route('order.removeAll',['id'=>$product->id])}}"><i
												class="fa fa-trash-o fa" aria-hidden="true"></i> Remove all</a></li>
							</ul>
						</div>
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
		<hr>
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<strong class="pull-right">Total: {{$cart->total}}&#8372;</strong>
			</div>
		</div>
		<hr>
		<h2>Shipping information</h2>
		{{Form::open(['url'=>route('cart')])}}
		@if(count($user->contacts))
			<div class="row">
				<div class="col-6">
					<div class="form-group">
						{{Form::label('contacts',"Contacts",['class'=>'form-control-label'])}}
						{{Form::select('contacts',$user->contacts->mapWithKeys(function ($contact){return
						[$contact->id=>"{$contact->city},
						{$contact->address}, phone {$contact->phone}"];}),
						null,
						['class'=>'form-control','placeholder'=>'Select contacts'])}}
					</div>
				</div>
			</div>
			<hr/>
		@endif
		<div class="row">
			<div class="col-6">
				<div class="form-group">
					{{Form::label('phone','Phone number',['class'=>'form-control-label'])}}
					{{Form::text('phone',null,['class'=>'form-control'])}}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-6">
				<div class="form-group">
					{{Form::label('city','City',['class'=>'form-control-label'])}}
					{{Form::text('city',null,['class'=>'form-control'])}}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-6">
				<div class="form-group">
					{{Form::label('address','Address',['class'=>'form-control-label'])}}
					{{Form::text('address',null,['class'=>'form-control'])}}
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-6">
				{{Form::submit('Checkout',['class'=>'btn btn-success'])}}
			</div>
		</div>
		{{Form::close()}}
	@else
		<div class="row">
			<div class="col-sm-12 col-md-12">
				Your cart is empty.
			</div>
		</div>
	@endif
@endsection