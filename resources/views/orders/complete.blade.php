@extends('layouts.master')

@section('title')
	Checkout
@endsection


@section('content')
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<h1>Payment successful ({{$order->total()}}&#8372;).</h1>
			<hr/>
			Thank you for your purchase!
		</div>
	</div>
@endsection