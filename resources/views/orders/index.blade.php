@extends('layouts.master')

@section('title')
	Shopping cart
@endsection
@section('content')
	<h1>Your cart</h1>
	@if(count($cart->items)>0)
		<ul class="list-group">
			@foreach($products as $product)
				<li class="list-group-item">
					<div class="row">
						<div class="col-sm-8 col-md-8">
							<strong>{{$product->title}} x {{$cart->items[$product->id]}}</strong>
							<div>
															<h4><span class="label
															label-success">{{$product->price*$cart->items[$product
															->id]}}
																&#8372;
												</span></h4>
							</div>
						</div>
						<div class="col-sm-4 col-md-4">
							<div class="btn-group pull-right">
								<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
								        aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-pencil"
									   aria-hidden="true"></i> Actions <i class="fa fa-caret-down"
									                                      aria-hidden="true"></i>
								</button>
								<ul class="dropdown-menu">
									<li><a href="{{route('order.add',['id'=>$product->id])}}"><i
													class="fa fa-plus"
													aria-hidden="true"></i> Add one</a></li>
									<li><a href="{{route('order.remove',['id'=>$product->id])}}"><i class="fa
									fa-minus"
									                   aria-hidden="true"></i> Remove one</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="{{route('order.removeAll',['id'=>$product->id])}}"><i
													class="fa fa-trash-o fa" aria-hidden="true"></i> Remove all</a></li>
								</ul>
							</div>
						</div>
					</div>
				</li>
			@endforeach
		</ul>
		<div class="row">
			<div class="col-sm-12 col-md-12">
				<strong class="pull-right">Total: {{$cart->total}}&#8372;</strong>
			</div>
		</div>
		<hr>
		<form method="post" action="{{route('checkout')}}">
			{{csrf_field()}}
			<button class="btn btn-success pull-right" type="submit">Checkout</button>
		</form>
	@else
		<div class="row">
			<div class="col-sm-12 col-md-12">
				Your cart is empty.
				</div>
		</div>
	@endif
@endsection