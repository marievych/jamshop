@extends('layouts.master')

@section('title')
	Order {{$order->id}}
	@endsection

@section('content')
	<h2>Order {{$order->id}}</h2>
	<table class="table table-striped">
		<thead>
		<tr>
			<th>Product</th>
			<th>Quantity</th>
			<th>Price</th>
			<th>Subtotal</th>
		</tr>
		</thead>
		<tbody>
		@foreach($order->items as $item)
			<tr>
				<td><a href="{{route('product.details',['id'=>$item->product_id])}}">{{$item->product->title}}</a></td>
				<td>{{$item->quantity}}</td>
				<td>{{$item->price}}&#8372;</td>
				<td>{{$item->quantity*$item->price}}&#8372;</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	<hr>
	<h3 class="pull-right">Total: {{$order->total()}}&#8372;</h3>
	@endsection