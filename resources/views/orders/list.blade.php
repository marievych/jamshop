@extends('layouts.master')

@section('title')
	Orders
	@endsection

@section('content')
	@if($orders)
	<table class="table">
		<thead>
		<tr>
			<th>Id</th>
			<th>Items</th>
			<th>Total</th>
		</tr>
		</thead>
		<tbody>
		@foreach($orders as $order)
		<tr>
			<td><a href="{{route('order.details',['id'=>$order->id])}}">{{$order->id}}</a></td>
			<td>
				<ul>
					@foreach($order->items as $item)
						<li><a href="{{route('product.details',['id'=>$item->product_id])
							}}">{{$item->product->title}}</a> x {{$item->quantity}}</li>
					@endforeach
				</ul>
			</td>
			<td>{{$order->total()}}&#8372;</td>
		</tr>
			@endforeach
		</tbody>
	</table>
		{{$orders->links('partials.pagination')}}
	@endif
	@endsection