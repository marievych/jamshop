@extends('layouts.master')

@section('title')
	Order {{$order->id}}
@endsection

@section('content')
	<h2>Order {{$order->id}}</h2>
	<div class="row">
		<div class="col-6">
			<b>Buyer</b>
		</div>
		<div class="col-6">
			<a href="{{route('admin.account.details',['id'=>$order->user_id])}}">{{$order->user->name}}</a>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-12">
			<div class="row">
				<div class="col-6"><b>Product</b></div>
				<div class="col-2"><b>Quantity</b></div>
				<div class="col-2"><b>Subtotal</b></div>
			</div>
			@foreach($order->items as $item)
				<div class="row">
					<div class="col-6"><a href="{{route('product.details',['id'=>$item->product_id])}}">{{$item->product->title}}</a></div>
					<div class="col-2">{{$item->quantity}}</div>
					<div class="col-2">{{$item->price*$item->quantity}}₴</div>
				</div>
				@endforeach
		</div>
	</div>
	<hr>
	@if($order->isPaid)
	<div class="row">
		<div class="col-6">Paid</div>
		<div class="col-6">{{$order->paymentDate}}</div>
	</div>
	@endif
	@if($order->isShipped)
		<div class="row">
			<div class="col-6">Shipped</div>
			<div class="col-6">{{$order->shipmentDate}}</div>
		</div>
	@endif
	<div class="row">
		<div class="col-6"><b>Total</b></div>
		<div class="col-6"><b>{{$order->total()}}₴</b></div>
	</div>
	<hr>
	<a href="{{route('admin.order.edit',['id'=>$order->id])}}" class="btn btn-success">Edit</a>
@endsection