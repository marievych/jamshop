@extends('layouts.master')

@section('title')
	Order {{$order->id}}
@endsection

@section('content')
	<h2>Order {{$order->id}}</h2>
{{Form::model($order)}}
	<div class="form-group">
		{{Form::label('status_id','Status',['class'=>'form-control-label'])}}
		{{Form::select('status_id',$statuses->pluck('name','id'),$order->status_id,['class'=>'form-control'])}}
	</div>
	{{Form::close()}}
@endsection