@extends('layouts.master')

@section('title')
	{{$user->name}} - Orders
	@endsection

@section('content')
		<table class="table">
			<thead>
			<tr>
				<th>Id</th>
				<th>Items</th>
				<th>Total</th>
			</tr>
			</thead>
			<tbody>
			@foreach($user->orders as $order)
				<tr>
					<td><a href="{{route('order.details',['id'=>$order->id])}}">{{$order->id}}</a></td>
					<td>
						<ul>
						@foreach($order->items as $item)
							<li><a href="{{route('product.details',['id'=>$item->product_id])
							}}">{{$item->product->title}}</a></li>
							@endforeach
						</ul>
					</td>
					<td>{{$order->total()}}&#8372;</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	@endsection