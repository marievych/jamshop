@extends('layouts.master')

@section('title')
	Accounts
@endsection

@section('content')
	<table class="table">
		<thead>
		<tr>
			<td>Id</td>
			<td>Name</td>
			<td>Roles</td>
			<td>Orders</td>
			<td></td>
		</tr>
		</thead>
		<tbody>
		@foreach($users as $user)
			<tr>
				<td>{{$user->id}}</td>
				<td><a href="{{route('admin.account.details',['id'=>$user->id])}}">{{$user->name}}</a></td>
				<td>
					<ul>
						@foreach($user->roles as $role)
							<li>{{$role->name}}</li>
						@endforeach
					</ul>
				</td>
				<td><a href="{{route('admin.account.orders',['id'=>$user->id])}}">{{count($user->orders)
				}}</a></td>
				<td></td>
			</tr>
		@endforeach
		</tbody>
	</table>
@endsection