@extends('layouts.master')

@section('title')
	Account - {{$user->name}}
@endsection
@section('content')
	<table class="table">
		<tbody>
		<tr>
			<th>Id</th>
			<td>{{$user->id}}</td>
		</tr>
		<tr>
			<th>Name</th>
			<td>{{$user->name}}</td>
		</tr>
		<tr>
			<th>Email</th>
			<td>{{$user->email}}</td>
		</tr>
		<tr>
			<th>Roles</th>
			<td>
				<ul>
					@foreach($user->roles as $role)
						<li>{{$role->name}}</li>
					@endforeach
				</ul>
			</td>
		</tr>
		</tbody>
	</table>
	<a href="{{route('admin.account.edit',['id'=>$user->id])}}" class="btn btn-success">Edit</a>
@endsection