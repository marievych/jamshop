<?/** @var App\Product $product */?>


@extends('layouts.master')

@section('title')
	Products
	@endsection

@section('content')
	<h2>Products</h2>
	<a href="{{route('admin.product.create')}}" class="btn btn-primary">Create product</a>
	<table class="table">
		<thead>
		<tr>
			<th>Id</th>
			<th>Title</th>
			<th>Active</th>
			<th>Created at</th>
			<th>Modified at</th>
			<th>Actions</th>
		</tr>
		</thead>
		<tbody>
		@foreach($products as $product)
			<tr>
				<td><a href="{{route('product.details',['id'=>$product->id])}}">{{$product->id}}</a></td>
				<td><a href="{{route('product.details',['id'=>$product->id])}}">{{$product->title}}</a></td>
				<td><div class="form-check disabled">
						<label class="form-check-label">
							<input class="form-check-input" type="checkbox" {{$product->active?'checked':''}}
							disabled>
						</label>
					</div></td>
				<td>{{$product->created_at->format('d.m.Y H:i:s')}}</td>
				<td>{{$product->updated_at->format('d.m.Y H:i:s')}}</td>
				<td>
					<a href="{{route('product.details',['id'=>$product->id])}}">Details</a><br>
					<a href="{{route('admin.product.edit',['id'=>$product->id])}}">Edit</a><br>
					<a href="{{route('admin.product.delete',['id'=>$product->id])}}">Delete</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	{{$products->links('partials.pagination')}}
	@endsection