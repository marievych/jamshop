@extends('layouts.master')

@section('title')
	Add new product
@endsection

@section('content')
	<div class="row">
		<div class="col-md-6 offset-md-3">
			{{$form=Form::model($product,['route' => 'admin.product.create','enctype'=>'multipart/form-data',
			'class'=>'form'])}}
			{{Form::token()}}
			<div class="form-group {{$errors->has('title')?'has-danger':''}}">
				{{Form::label('title','Title',['class'=>'form-control-label'])}}
				{{Form::text('title',null,['class'=>'form-control'])}}
			</div>
			<div class="form-group {{$errors->has('description')?'has-danger':''}}">
				{{Form::label('description','Description',['class'=>'form-control-label'])}}
				{{Form::textarea('description',null,['class'=>'form-control'])}}
			</div>
			<div class="form-group {{$errors->has('price')?'has-danger':''}}">
				{{Form::label('price','Price',['class'=>'form-control-label'])}}
				<div class="input-group">
					{{Form::number('price',null,['class'=>'form-control','step'=>0.01])}}
					<span class="input-group-addon">&#8372;</span>
				</div>
			</div>
			<div class="form-group">
				{{Form::label('image','Image',['class'=>'form-control-label'])}}
				<div><img src="{{$product->image}}" class="thumbnail"/></div>
				{{Form::file('image',null,['class'=>'form-control'])}}
			</div>
			<div class="form-group">
				<div class="form-check">
					<label class="form-check-label">
						{{Form::checkbox('active',true,null,['class'=>'form-check-input'])}}
						Active</label>
				</div>
			</div>
			<button class="btn btn-success" type="submit">{{Request::route()->getName()=='admin.product
		.create'?'Add':'Save'}}</button>
			{{Form::close()}}
		</div>
	</div>
@endsection