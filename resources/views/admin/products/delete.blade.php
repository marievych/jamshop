<?/** @var App\Product $product */?>

@extends('layouts.master')

@section('title')
	Delete {{$product->title}} [{{$product->id}}]
@endsection


@section('content')
	<div class="row">
		<div class="col-sm-12 col-md-12">
			<h2>Delete {{$product->title}} [{{$product->id}}]?</h2>
			{{Form::open()}}
			<div class="form-group">
				<p>Are you sure you want to delete {{$product->title}} [{{$product->id}}]?</p>
			</div>
			{{Form::submit('Delete',['class'=>'btn btn-danger'])}}
			<a href="{{URL::previous()}}" class="btn btn-primary">Cancel</a>
			{{Form::close()}}
		</div>
	</div>
@endsection