@extends('layouts.master')

@section('title')
	{{$product->title}} [{{$product->id}}]
@endsection

@section('content')
	<h2>{{$product->title}} [{{$product->id}}]</h2>
	<div class="row">
		<div class="col-lg-2 col-md-3">
			<img class="thumbnail" src="{{$product->image}}"/>
		</div>
		<div class="col-lg-10 col-md-9">
			<div class="row">
				<div class="col-12">
					{{$product->description}}
				</div>
			</div>
		</div>
	</div>
	<hr>
	<div class="row">
		<div class="col-12">
			<div class="col-12"><h3 class="pull-right">{{$product->price}}₴</h3></div>
		</div>
	</div>
	<hr>
	@if(Auth::user()&&Auth::user()->isAdmin())
		<div class="row">
			<div class="col-12">
					<a href="{{route('admin.product.edit',['id'=>$product->id])}}" class="btn btn-success">Edit</a>
					<a href="{{route('admin.product.delete',['id'=>$product->id])}}" class="btn btn-danger">Delete</a>
			</div>
		</div>
	@endif
	{{--<table class="table">
		<tr>
			<td><img class="thumbnail" src="{{$product->image}}"/></td>
			<td>
				<div>{{$product->description}}</div>
				<hr/>
				<div class="pull-right"><h3>{{$product->price}}&#8372;</h3></div>
			</td>
		</tr>
		@if(Auth::user()&&Auth::user()->isAdmin())
			<tr>
				<td>
					<a href="{{route('admin.product.edit',['id'=>$product->id])}}" class="btn btn-success">Edit</a>
					<a href="#" class="btn btn-danger">Delete</a>
				</td>
				<td></td>
			</tr>
		@endif
	</table>--}}
@endsection