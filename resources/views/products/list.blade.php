@extends('layouts.master')

@section('title')
	Jam shop
@endsection

@section('content')

	
	
	<div class="card-columns">
		@foreach($products as $product)
				<div class="card">
					<div class="card-block">
						<div class="card-title"><h4>{{$product->title}}</h4></div>
						<p class="description card-text">
							<img class="img-thumbnail pull-left" alt="{{$product->title}}"
							     src="{{$product->image}}">{{$product->description}}</p>
						<div class="clearfix">
							<div class="pull-left price">{{$product->price}}₴</div>
							<a class="btn btn-success pull-right" role="button"
							   href="{{route('order.add',['id'=>$product->id])}}"><i class="fa fa-cart-plus fa-lg"
							                                                         aria-hidden="true"></i> Add to
								cart</a>
						</div>
					</div>
				</div>
		@endforeach
	</div>
@endsection