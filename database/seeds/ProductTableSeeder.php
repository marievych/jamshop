<?php

use App\Product;
use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $product=new Product([
        	'image'=>'http://www.homemade-gifts-made-easy.com/image-files/homemade-strawberry-jam-recipe-500x668.jpg',
	        'title'=>'Strawberry jam',
	        'price'=>95.99,
	        'description'=>'Strawberry jam is one of the ultimate culinary nectars, the result of boiling down fresh seasonal fruit into a bold, jewel-toned lava just bursting with concentrated berry flavor. Adults love it, kids eat it nearly every day for lunch, and babies always have it all over their faces. Slather it on bread and butter, sandwich it with peanut butter, spread it on a hot English muffin, or stir it into yogurt. However you apply it, it’s one of those classic down-home comfort foods that can make just about anything taste better.'
        ]);
        $product->save();
	    $product=new Product([
		    'image'=>'http://www.homemade-gifts-made-easy.com/image-files/homemade-strawberry-jam-recipe-500x668.jpg',
		    'title'=>'Strawberry jam',
		    'price'=>95.99,
		    'description'=>'Strawberry jam is one of the ultimate culinary nectars, the result of boiling down fresh seasonal fruit into a bold, jewel-toned lava just bursting with concentrated berry flavor. Adults love it, kids eat it nearly every day for lunch, and babies always have it all over their faces. Slather it on bread and butter, sandwich it with peanut butter, spread it on a hot English muffin, or stir it into yogurt. However you apply it, it’s one of those classic down-home comfort foods that can make just about anything taste better.'
	    ]);
	    $product->save();
	    $product=new Product([
		    'image'=>'http://www.homemade-gifts-made-easy.com/image-files/homemade-strawberry-jam-recipe-500x668.jpg',
		    'title'=>'Strawberry jam',
		    'price'=>95.99,
		    'description'=>'Strawberry jam is one of the ultimate culinary nectars, the result of boiling down fresh seasonal fruit into a bold, jewel-toned lava just bursting with concentrated berry flavor. Adults love it, kids eat it nearly every day for lunch, and babies always have it all over their faces. Slather it on bread and butter, sandwich it with peanut butter, spread it on a hot English muffin, or stir it into yogurt. However you apply it, it’s one of those classic down-home comfort foods that can make just about anything taste better.'
	    ]);
	    $product->save();
	    $product=new Product([
		    'image'=>'http://www.homemade-gifts-made-easy.com/image-files/homemade-strawberry-jam-recipe-500x668.jpg',
		    'title'=>'Strawberry jam',
		    'price'=>95.99,
		    'description'=>'Strawberry jam is one of the ultimate culinary nectars, the result of boiling down fresh seasonal fruit into a bold, jewel-toned lava just bursting with concentrated berry flavor. Adults love it, kids eat it nearly every day for lunch, and babies always have it all over their faces. Slather it on bread and butter, sandwich it with peanut butter, spread it on a hot English muffin, or stir it into yogurt. However you apply it, it’s one of those classic down-home comfort foods that can make just about anything taste better.'
	    ]);
	    $product->save();
	    $product=new Product([
		    'image'=>'http://www.homemade-gifts-made-easy.com/image-files/homemade-strawberry-jam-recipe-500x668.jpg',
		    'title'=>'Strawberry jam',
		    'price'=>95.99,
		    'description'=>'Strawberry jam is one of the ultimate culinary nectars, the result of boiling down fresh seasonal fruit into a bold, jewel-toned lava just bursting with concentrated berry flavor. Adults love it, kids eat it nearly every day for lunch, and babies always have it all over their faces. Slather it on bread and butter, sandwich it with peanut butter, spread it on a hot English muffin, or stir it into yogurt. However you apply it, it’s one of those classic down-home comfort foods that can make just about anything taste better.'
	    ]);
	    $product->save();
	    $product=new Product([
		    'image'=>'http://www.homemade-gifts-made-easy.com/image-files/homemade-strawberry-jam-recipe-500x668.jpg',
		    'title'=>'Strawberry jam',
		    'price'=>95.99,
		    'description'=>'Strawberry jam is one of the ultimate culinary nectars, the result of boiling down fresh seasonal fruit into a bold, jewel-toned lava just bursting with concentrated berry flavor. Adults love it, kids eat it nearly every day for lunch, and babies always have it all over their faces. Slather it on bread and butter, sandwich it with peanut butter, spread it on a hot English muffin, or stir it into yogurt. However you apply it, it’s one of those classic down-home comfort foods that can make just about anything taste better.'
	    ]);
	    $product->save();
    }
}
