<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Statuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statuses',function (Blueprint $table){
        $table->increments('id');
        $table->string('name');
        });
        
        Schema::table('orders',function (Blueprint $table){
        	$table->integer('status_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statuses');
        Schema::table('orders',function (Blueprint $table){
        	$table->dropColumn('status_id');
        });
    }
}
