<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_items', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
	        $table->integer('product_id');
            $table->integer('cart_id');
            $table->decimal('price',10,2);
            $table->integer('quantity');
        });
        
        Schema::create('cart_item_to_product',function (Blueprint $table){
	        $table->timestamps();
	        $table->integer('cart_item_id');
	        $table->integer('product_id');
	        $table->primary(['cart_item_id','product_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_items');
	    Schema::dropIfExists('cart_item_to_product');
    }
}
