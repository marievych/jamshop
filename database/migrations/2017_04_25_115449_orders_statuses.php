<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrdersStatuses extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('statuses');
        Schema::table('orders',function (Blueprint $table){
        	$table->dropColumn('status_id');
        	$table->boolean('is_paid');
        	$table->timestamp('payment_date')->nullable();
        	$table->boolean('is_shipped');
        	$table->timestamp('shipment_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::create('statuses',function (Blueprint $table){
		    $table->increments('id');
		    $table->string('name');
	    });
	    Schema::table('orders',function (Blueprint $table){
		    $table->integer('status_id');
		    $table->dropColumn('is_paid');
		    $table->dropColumn('payment_date');
		    $table->dropColumn('is_shipped');
		    $table->dropColumn('shipment_date');
	    });
    }
}
