<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts',function (Blueprint $table){
        	$table->dropColumn(['street','building','apartment']);
        	$table->string('address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts',function (Blueprint $table){
        	$table->dropColumn('address');
        	$table->string('street');
        	$table->string('building');
        	$table->string('apartment');
        });
    }
}
