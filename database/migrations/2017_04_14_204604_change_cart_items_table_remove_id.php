<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeCartItemsTableRemoveId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cart_items',function (Blueprint $table){
        	//$table->dropPrimary('id');
        	$table->dropColumn('id');
        	$table->primary(['order_id','product_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cart_items',function (Blueprint $table){
        	$table->dropPrimary(['order_id','product_id']);
        	$table->increments('id');
        	$table->primary('id');
        });
    }
}
