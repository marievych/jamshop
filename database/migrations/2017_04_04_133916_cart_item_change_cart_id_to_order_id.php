<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CartItemChangeCartIdToOrderId extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cart_items', function (Blueprint $table) {
			$table->dropColumn('cart_id');
			$table->integer('order_id');
		});
		Schema::table('orders', function (Blueprint $table) {
			$table->dropColumn('cart_id');
		});
	}
	
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cart_items', function (Blueprint $table) {
			$table->dropColumn('order_id');
			$table->integer('cart_id');
		});
		Schema::table('orders', function (Blueprint $table) {
			$table->integer('cart_id');
		});
	}
}
